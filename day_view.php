<?php
// Initialize the session
session_start();
require_once "./Login/config.php";
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ./login/login.php");
    exit;
}
?>
<html>
<?php
   if(isset($_REQUEST['date'])){
    $day = date('d', strtotime($_REQUEST['date']));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime($_REQUEST['date']));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime($_REQUEST['date']));      //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime("01-" . $month . "-" . $year));  //Gets the day of the week for the 1st of  
                  //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime( $_REQUEST['date']));      //Gets number of days in month 
    $nmonth = strtotime($_REQUEST['date']);
   }else{
    $day = date('d', strtotime(date("Y-m-d") ));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime(date("Y-m-d")));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime(date("Y-m-d") ));      //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime("01-" . $month . "-" . $year));  //Gets the day of the week for the 1st of  
                  //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime( date("Y-m-d")));      //Gets number of days in month 
    $nmonth = strtotime(date("Y-m-d"));
   }
   $now= $year."-".$month."-".$day ;
   $user_id =   $_SESSION["id"];
   $sql = "SELECT apm_id,apm_text,apm_start,apm_end FROM appointment WHERE $user_id = apm_user AND apm_date='$now'" ;
   $title = mysqli_query($link, $sql); //Gets appointment title 
   $today = date('d');            //Gets today’s date 
   $todaymonth = date('m');          //Gets today’s month 
   $todayyear = date('Y');            //Gets today’s year 


   $apm_text = array();
   $apm_id = array();
   $apm_start = array();
   $apm_end = array();
   
   if ($title!="") {
   while($row = mysqli_fetch_assoc($title)){

    array_push($apm_text,$row["apm_text"]);
    array_push($apm_id,$row["apm_id"]);
    array_push($apm_start,$row["apm_start"]);
    array_push($apm_end,$row["apm_end"]);
   }
}
    $dayname = date('D', strtotime($year."-".$month."-".$day));
    $month_name = date("F", mktime(null, null, null, $month)); //change number to name month
	$next_day = date('Y-m-d',strtotime('+1 day', $nmonth));
    $prev_day = date('Y-m-d',strtotime('-1 day', $nmonth));
    
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
    html {
        padding: 1%;
    }

    .time {
        margin-bottom: 2%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .calendar {
        position: relative;
        width: 960px;
        margin-left: 8%;
        height: 100vh;
        background: white;
        position: relative;
        width: 960px;
        margin-left: 8%;
        height: 70vh;
        overflow: auto;
        background: white;
        padding: 1.5%;

    }

    div.date,
    div.days {
        width: 120px;
        border: 1px solid black;
        float: left;
        margin: 1px;
    }

    .blankday {
        background: #ccc;
    }

    div.date {
        height: 78px;
    }

    .today {
        background: #cfc;
    }

    .modal {

        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
        background-image: url(https://i.pinimg.com/originals/24/88/ff/2488ff1e482f4ee588382381bdc19123.jpg);
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .first {
        display: grid;
        grid-template-columns: 1fr 2.1fr 0.9fr;
        column-gap: 20px;
        row-gap: 10px;
        grid-template-rows: 50px auto 30px;
        grid-template-areas:
            'name month username'
            'calendar calendar calendar'
    }

    .item-name {
        grid-area: name;
        font-weight: bold;
        font-size: 1.5em;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }

    .item-month {
        grid-area: month;
        font-weight: bold;
        font-size: 2em;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .item-username {
        grid-area: username;
        font-weight: bold;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .item-calendar {
        grid-area: calendar;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;

    }

    body {
        background-image: url(https://gobrief.com/wp-content/uploads/minimalism-can-reduce-stress-brief.jpg);
    }

    .menu {
        position: absolute;
        left: 0;
        width: 150px;
        height: 300px;
        background-color: #ffffff;
        opacity: 0.75;
        border: 1px solid black;
    }

    hr {
        width: 100%;
        margin-left: 2%;
    }
        /* width */
        ::-webkit-scrollbar {
        width: 5px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #888;
    }
    </style>
</head>


<body>
    <div class="first">
        <div class="item-name">
            My Calendar profile
        </div>
        <div class="item-month">
            <a href="?date=<?php echo $prev_day; ?>">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-left-fill" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M3.86 8.753l5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z" />
                </svg>
            </a>
            <?php
                echo $dayname." ".$day." ,".$month_name." ".$year;
            ?>
            <a href="?date=<?php echo $next_day; ?>">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
                </svg>
            </a>
        </div>
        <div class="item-username">
            <?php
                echo "Username : ".$_SESSION["username"];
            ?>
        </div>
        <div class="item-calendar">
            <div class="add-amp">
                <form action="./Appointment/add_apm_dayview.php" method="POST">
                    <input type="text" name="apm">
                    <input type="date" name="apm_date">
                    <input type="time" name="apm_start">
                    <input type="time" name="apm_end">
                    <input type="submit" value="submit" class="btn btn-secondary">
                </form>
            </div>
            <div class="menu">
                <div style="font-size: 1rem; margin: 10px;margin-top: 25px;"><a href="./day_view.php">TODAY</a></div>
                <div style="font-size: 1rem; margin: 10px;"><a href="#">DAY VIWE</a></div>
                <div style="font-size: 1rem;margin: 10px;"><a href="./week_view.php?date=<?php echo $year."-".$month."-".$day;?>">WEEK VIWE</a></div>
                <div style="font-size: 1rem;margin: 10px;">
                    <a href="./index.php?date=<?php echo $year."-".$month."-".$day;?>">MONTH VIWE</a>
                </div>
                <a href="./login/logout.php" class="btn btn-secondary active" role="button"
                    style="margin: 10px; width: 85%; position: absolute; bottom: 0px;">
                    Logout
                </a>
            </div>
            <div class="calendar"> 
                <?php
                    for($i=0; $i<24; $i++){
                        if($i<10){
                            echo '<div class="time">0'.$i.':00 <hr/></div>';
                            echo '<div class="time">0'.$i.':30 <hr/></div>';
                        }else{
                            echo '<div class="time">'.$i.':00 <hr/></div>';
                            echo '<div class="time">'.$i.':30 <hr/></div>';
                        }

                    }
                ?>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal">

        <div class="modal-content">
            <span class="close">&times;</span>
            <div id="apm"></div>
            <div id="start"></div>
            <div id="end"></div>
            <form action="./Appointment/delete_apm_dayview.php" method="POST">
                <input type="text" name="id" id="id" hidden>
                <input type="date" name="apm_date" id="apm_date" hidden>
                <input type="submit" value="Delete" class="btn btn-outline-danger">
            </form>
        </div>

    </div>
</body>
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
function pop(title, id, date, start, end) {
    modal.style.display = "block";
    document.getElementById("apm").innerHTML = title;
    document.getElementById("id").value = id;
    document.getElementById("apm_date").value = date;
    document.getElementById("start").innerHTML = start;
    document.getElementById("end").innerHTML = end;
}

//When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

var apm_id = [];
var apm_text = [];
var apm_start = [];
var apm_end = [];
var date = '';

//insert value from php to javascript
<?php
    for($i=0;$i<sizeof($apm_id);$i++){
        echo 'apm_id.push('.$apm_id[$i].');';
        echo 'apm_text.push("'.$apm_text[$i].'");';
        echo 'apm_start.push("'.$apm_start[$i].'");';
        echo 'apm_end.push("'.$apm_end[$i].'");';
    }

    echo 'date = " '.$month.' '.$day.','.$year.' "';

?>

var apm_top //get start time(top position)
var apm_bottom //get end time(height) 
var apm_h //calulate height
var apm_left = document.getElementsByTagName('hr')[0].offsetLeft + 20 //get left position

for (var i = 0; i < apm_id.length; i++) {
    var time_start = new Date(date + apm_start[i]);
    var time_end = new Date(date + apm_end[i]);
    var hr_start = time_start.getHours();
    var min_start = time_start.getMinutes();
    var hr_end = time_end.getHours();
    var min_end = time_end.getMinutes();
    var index_top = 0;
    var index_end = 0;

    //loop get top position
    for (var h = 0; h < 24; h++) {
        if (hr_start == h) {
            if (min_start == 0) {
                apm_top = document.getElementsByTagName('hr')[index_top].offsetTop
            } else if (min_start > 0 && min_start < 30) {
                apm_top = document.getElementsByTagName('hr')[index_top].offsetTop + 30;
            } else if (min_start == 30) {
                apm_top = document.getElementsByTagName('hr')[index_top + 1].offsetTop
            } else {
                apm_top = document.getElementsByTagName('hr')[index_top + 1].offsetTop + 30
            }

        }

        index_top = index_top + 2;
    }

    //loop get end position
    for (var h = 0; h < 24; h++) {
        if (hr_end == h) {
              if (min_end == 0) {
                apm_bottom = document.getElementsByTagName('hr')[index_end].offsetTop
            } else if (min_end > 0 && min_end < 30) {
                apm_bottom = document.getElementsByTagName('hr')[index_end].offsetTop + 30;
            } else if (min_end == 30) {
                apm_bottom = document.getElementsByTagName('hr')[index_end + 1].offsetTop
            } else {
                apm_bottom = document.getElementsByTagName('hr')[index_end + 1].offsetTop + 30;
            }
        }
        index_end = index_end + 2;
    }

    apm_h = apm_bottom - apm_top //calculate height

    var apm = document.createElement("DIV"); //create div element
    apm.style.width = "150px"; //set width of div
    apm.style.height = apm_h + "px"; //set height of div
    apm.style.background = "red"; //set bg-color
    apm.style.top = apm_top + "px"; //set top position
    apm.style.left = apm_left + "px"; //set left position
    apm.style.color = "white"; //set font color
    apm.style.padding = "1%"; //set padding
    apm.style.position = "absolute"; //set position
    apm.style.textOverflow = "ellipsis";
    apm.classList.add("apm");

    var txt = apm_text[i];
    var index = i;
    apm.innerHTML = "<b>"+apm_text[i]+"</b><br>"+hr_start+":"+min_start+" - "+hr_end+":"+min_end;
    document.getElementsByClassName("calendar")[0].appendChild(apm); //make div appear  

}

<?php 
for($i=0;$i<sizeof($apm_text);$i++){
    echo 'date = "'.$year.'-'.$month.'-'.$day.'";';
    echo ' document.getElementsByClassName("apm")['.$i.'].onclick = function(){ pop(apm_text['.$i.'],apm_id['.$i.'],date,apm_start['.$i.'],apm_end['.$i.'] ); };';
}
?>
 
</script>

</html>
<?php
// Initialize the session
session_start();
require_once "./Login/config.php";
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ./login/login.php");
    exit;
}
?>
<html>
<?php
   if(isset($_REQUEST['date'])){
    $day = date('d', strtotime($_REQUEST['date']));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime($_REQUEST['date']));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime($_REQUEST['date']));      //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime("01-" . $month . "-" . $year));  //Gets the day of the week for the 1st of  
                  //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime( $_REQUEST['date']));      //Gets number of days in month 
    $nmonth = strtotime($_REQUEST['date']);
   }else{
    $day = date('d', strtotime(date("Y-m-d") ));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime(date("Y-m-d")));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime(date("Y-m-d") ));      //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime("01-" . $month . "-" . $year));  //Gets the day of the week for the 1st of  
                  //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime( date("Y-m-d")));      //Gets number of days in month 
    $nmonth = strtotime(date("Y-m-d"));
   }
  
   $user_id =   $_SESSION["id"];
   $sql = "SELECT apm_id,apm_text, apm_date FROM appointment WHERE $user_id = apm_user " ;
   $title = mysqli_query($link, $sql); //Gets appointment title 
   $today = date('d');            //Gets today’s date 
   $todaymonth = date('m');          //Gets today’s month 
   $todayyear = date('Y');            //Gets today’s year 

   $apm_date = array();
   $apm_text = array();
   $apm_id = array();
   
   if($title!=""){
        while($row = mysqli_fetch_assoc($title)){
            array_push($apm_date, $row["apm_date"]);
            array_push($apm_text,$row["apm_text"]);
            array_push($apm_id,$row["apm_id"]);
        }
   }


    $month_name = date("F", mktime(null, null, null, $month)); //change number to name month
	$next_month = date('Y-m-d',strtotime('+1 month', $nmonth));
    $prev_month = date('Y-m-d',strtotime('-1 month', $nmonth));
    
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
    html {
        padding: 1%;
    }

    .calendar {
        position: relative;
        width: 960px;
        margin-left: 8%;
    }

    div.date,
    div.days {
        width: 120px;
        border: 1px solid black;
        float: left;
        margin: 1px;
    }

    .blankday {
        background: #ccc;
    }

    div.date {
        height: 78px;
        overflow: auto;
    }

    .today {
        background: #cfc;
    }

    .modal {

        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
        
    }

    /* Modal Content */
    .modal-content {
        background-image: url(https://www.photos-public-domain.com/wp-content/uploads/2012/01/crumpled-yellow-paper-texture.jpg);
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .first {
        display: grid;
        grid-template-columns: 1fr 2.1fr 0.9fr;
        column-gap: 20px;
        row-gap: 10px;
        grid-template-rows: 50px auto 30px;
        grid-template-areas:
            'name month username'
            'calendar calendar calendar'
            'time time time';

    }

    .item-name {
        grid-area: name;
        font-weight: bold;
        font-size: 1.5em;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }

    .item-month {
        grid-area: month;
        font-weight: bold;
        font-size: 2em;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .item-username {
        grid-area: username;
        font-weight: bold;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .item-calendar {
        grid-area: calendar;
        color:black;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
       
       
        
    }

    .item-time {
        grid-area: time;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: #ffffff;
        opacity: 0.8;
        border: 1px solid black;
    }

    body {
        background-image: url(https://i.pinimg.com/originals/24/88/ff/2488ff1e482f4ee588382381bdc19123.jpg);
    }

    .menu {
        position: absolute;
        left: 0;
        width: 150px;
        height: 300px;
        background-color: #ffffff;
        opacity: 0.75;
        border: 1px solid black;
        
    }
        /* width */
        ::-webkit-scrollbar {
        width: 5px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #888;
    }
    </style>
</head>


<body>
    <div class="first">
        <div class="item-name">
            My Calendar profile
        </div>
        <div class="item-month">
            <a href="?date=<?php echo $prev_month; ?>">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-left-fill" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M3.86 8.753l5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z" />
                </svg>
            </a>
            <?php
                echo $month_name." ".$year;
            ?>
            <a href="?date=<?php echo $next_month; ?>">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
                </svg>
            </a>
        </div>
        <div class="item-username">
            <?php
                echo "Username : ".$_SESSION["username"];
            ?>
        </div>
        <div class="item-calendar">
            <div class="add-amp">
                <form action="./Appointment/add_apm.php" method="POST">
                    <input type="text" name="apm">
                    <input type="date" name="apm_date">
                    <input type="time" name="apm_start">
                    <input type="time" name="apm_end">
                    <input type="submit" value="submit" class="btn btn-secondary">
                </form>
            </div>
            <div class="menu">
                <div style="font-size: 1rem; margin: 10px;margin-top: 25px;"><a href="./index.php">TODAY</a></div>
                <div style="font-size: 1rem; margin: 10px;"><a href="./day_view.php?date=<?php echo $year."-".$month."-".$day;?>">DAY VIWE</a></div>
                <div style="font-size: 1rem;margin: 10px;"><a href="./week_view.php?date=<?php echo $year."-".$month."-".$day;?>">WEEK VIWE</a></div>
                <div style="font-size: 1rem;margin: 10px;"><a href="#">MONTH VIWE</a></div>
                <a href="./login/logout.php" class="btn btn-secondary active" role="button" style="margin: 10px; width: 85%; position: absolute; bottom: 0px;">
                    Logout
                </a>
            </div>
            <div class="calendar">
                <div class="days">Sunday</div>
                <div class="days">Monday</div>
                <div class="days">Tuesday</div>
                <div class="days">Wednesday</div>
                <div class="days">Thursday</div>
                <div class="days">Friday</div>
                <div class="days">Saturday</div>
                <?php 
                    for($i=1; $i<=$firstday; $i++) 
                    { 
                        echo '<div class="date blankday"></div>'; 
                    } 
                ?>
                <?php 
                    for($i=1; $i<=$days; $i++) 
                    { 
                        if($i<10){
                            $today_date = $year."-".$month."-0".$i;
                            $today_date2 = '"'.$today_date.'"';
                        }else{
                            $today_date = $year."-".$month."-".$i;
                            $today_date2 = '"'.$today_date.'"';
                        }
                        
                        echo '<div class="date'; 
                        if ($today == $i && $todaymonth==$month && $todayyear == $year) 
                        { 
                            echo ' today'; 
                        } 
                        echo '">' . $i . '<br>'; 
                        for($j=0;$j<sizeof($apm_date);$j++){
                            if($today_date == $apm_date[$j]){
                                $title = '"'.$apm_text[$j].'"'; 
                                $id = $apm_id[$j];

                                echo  "<div onclick='pop(".$title.", ".$id.",".$today_date2.")'>".$apm_text[$j]."</div>";

                            }
                        }

                        echo  '</div>'; 
                    } 
                ?>
                <?php 
                     $daysleft = 7-(($days + $firstday)%7); 
                     if($daysleft<7) 
                      { 
                        for($i=1; $i<=$daysleft; $i++) 
                        { 
                         echo '<div class="date blankday"></div>'; 
                        } 
                      } 
                ?>
            </div>
        </div>
        <div class="item-time">
            <div id="time"></div>
        </div>
    </div>

    <div id="myModal" class="modal">

        <div class="modal-content">
            <span class="close">&times;</span>
            <div id="apm"></div>
            <form action="./Appointment/delete_apm.php" method="POST">
                <input type="text" name="id" id="id" hidden>
                <input type="date" name="apm_date" id="apm_date" hidden>
                <input type="submit" value="Delete" class="btn btn-outline-danger">
            </form>
        </div>

    </div>
</body>
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
function pop(title, id, date) {
    modal.style.display = "block";
    document.getElementById("apm").innerHTML = title;
    document.getElementById("id").value = id;
    document.getElementById("apm_date").value = date;

}

//When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function Timer() {
    var dt = new Date();
    var hour = dt.getHours();
    var minute = dt.getMinutes();
    var second;
    if (dt.getSeconds() < 10) {
        second = "0" + dt.getSeconds();
    } else {
        second = dt.getSeconds();
    }
    document.getElementById('time').innerHTML = hour + ":" + minute + ":" + second;
    setInterval("Timer()", 1000);
}
Timer();
</script>

</html>
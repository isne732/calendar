<?php
// Initialize the session
session_start();
require_once "./Login/config.php";
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ./login/login.php");
    exit;
}
?>
<html>
<?php
   if(isset($_REQUEST['date'])){
    $day = date('d', strtotime($_REQUEST['date']));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime($_REQUEST['date']));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime($_REQUEST['date']));      //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime("01-" . $month . "-" . $year));  //Gets the day of the week for the 1st of  
                  //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime( $_REQUEST['date']));      //Gets number of days in month 
    $nmonth = strtotime($_REQUEST['date']);
   }else{
    $day = date('d', strtotime(date("Y-m-d") ));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime(date("Y-m-d")));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime(date("Y-m-d") ));      //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime("01-" . $month . "-" . $year));  //Gets the day of the week for the 1st of  
                  //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime( date("Y-m-d")));      //Gets number of days in month 
    $nmonth = strtotime(date("Y-m-d"));
   }
   $now= $year."-".$month."-".$day ;
   $user_id =   $_SESSION["id"];

   $today = date('d');            //Gets today’s date 
   $todaymonth = date('m');          //Gets today’s month 
   $todayyear = date('Y');            //Gets today’s year 

   $all_title = array();
   $all_id=array();
   $all_start=array();
   $all_end=array();
   $all_date=array();

    $dayname = date('D', strtotime($year."-".$month."-".$day));
    $month_name = date("F", mktime(null, null, null, $month)); //change number to name month

   
   
   $minus = $day+1;
   $plus = $days-$day+1;

   $firstday_week=array();
   for($i=1; $i<=$days;$i++){
       if($i%7==1){
           array_push($firstday_week,$i);
       }
   }
   
   $weekindex=0; 
   for($i=0;$i<sizeof($firstday_week);$i++){
       if($day>=$firstday_week[$i]){
           $weekindex=$i;
       }
   }
   $day=$firstday_week[$weekindex];

   if($weekindex<sizeof($firstday_week)-1){
       $next_week = date("Y-m-d",strtotime("+7 day", $nmonth ));             
   }else{
       $next_week = date("Y-m-d",strtotime("+$plus day", $nmonth ));
   }

   if($weekindex==0){
       $prev_week = date("Y-m-d",strtotime("-$minus day",  $nmonth));
   }else{
       $prev_week = date("Y-m-d",strtotime("-7 day",  $nmonth));
   }

	$day_color=array(
        "red",
        "yellow",
        "pink",
        "green",
        "orange",
        "blue",
        "purple"
    );
    
    
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
    /* width */
    ::-webkit-scrollbar {
        width: 5px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #888;
    }

    html {
        padding: 1%;
    }

    .time {
        margin-bottom: 2%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .calendar {
        position: absolute;
        width: 20%;
        top: 24%;
        left: 13%;
        height: 70vh;
        overflow: auto;
        background: white;
        padding: 1.5%;
    }

    div.date,
    div.days {
        width: 120px;
        border: 1px solid black;
        float: left;
        margin: 1px;
    }

    .blankday {
        background: #ccc;
    }

    div.date {
        height: 78px;
    }

    .today {
        background: #cfc;
    }

    .modal {

        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
        background-image: url(https://i.pinimg.com/originals/24/88/ff/2488ff1e482f4ee588382381bdc19123.jpg);
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .first {
        display: grid;
        grid-template-columns: 1fr 2.1fr 0.9fr;
        column-gap: 20px;
        row-gap: 10px;
        grid-template-rows: 50px auto 30px;
        grid-template-areas:
            'name month username'
            'calendar calendar calendar'
    }

    .item-name {
        grid-area: name;
        font-weight: bold;
        font-size: 1.5em;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }

    .item-month {
        grid-area: month;
        font-weight: bold;
        font-size: 2em;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .item-username {
        grid-area: username;
        font-weight: bold;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .item-calendar {
        grid-area: calendar;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;

    }

    body {
        background-image: url(https://gobrief.com/wp-content/uploads/minimalism-can-reduce-stress-brief.jpg);
    }

    .menu {
        position: fixed;
        left: 0;
        top: 30%;
        width: 150px;
        height: 300px;
        background-color: #ffffff;
        opacity: 0.75;
    }

    hr {
        width: 100%;
        margin-left: 2%;
    }
    </style>
</head>


<body>
    <div class="first">
        <div class="item-name">
            My Calendar profile
        </div>
        <div class="item-month">
            <a href="?date=<?php echo $prev_week; ?>">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-left-fill" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M3.86 8.753l5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z" />
                </svg>
            </a>
            <?php
                echo $month_name." ".$year;
            ?>
            <a href="?date=<?php echo $next_week; ?>">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
                </svg>
            </a>
        </div>
        <div class="item-username">
            <?php
                echo "Username : ".$_SESSION["username"];
            ?>
        </div>
        <div class="item-calendar">
            <div class="add-amp">
                <form action="./Appointment/add_apm_weekview.php" method="POST">
                    <input type="text" name="apm">
                    <input type="date" name="apm_date">
                    <input type="time" name="apm_start">
                    <input type="time" name="apm_end">
                    <input type="submit" value="submit" class="btn btn-secondary">
                </form>
            </div>
            <div class="menu">
                <div style="font-size: 1rem; margin: 10px;margin-top: 25px;"><a href="./week_view.php">TODAY</a></div>
                <div style="font-size: 1rem; margin: 10px;"><a
                        href="./day_view.php?date=<?php echo $year."-".$month."-".$day;?>">DAY VIWE</a></div>
                <div style="font-size: 1rem;margin: 10px;"><a href="#">WEEK VIWE</a></div>
                <div style="font-size: 1rem;margin: 10px;">
                    <a href="./index.php?date=<?php echo $year."-".$month."-".$day;?>">MONTH VIWE</a>
                </div>
                <a href="./login/logout.php" class="btn btn-secondary active" role="button"
                    style="margin: 10px; width: 85%; position: absolute; bottom: 0px;">
                    Logout
                </a>
            </div>

            <?php
               
                ?>

            <?php
                                $index = -1;
                                $count = 0;
                                for($x=$day;$x<=$day+6 ;$x++){
                                    $margin_left = $count*21;
                                    $count++;
                                    if($x<=$days){
                                        $index++;
                                        $dayname = date('D', strtotime($year."-".$month."-".$x));
                                        $apm_date="$year-$month-$x";
                                        $sql = "SELECT apm_date,apm_id,apm_text,apm_start,apm_end FROM appointment WHERE $user_id = apm_user AND apm_date='$apm_date'" ;
                                        $title = mysqli_query($link, $sql);
                                        $app_title = array();
                                        $app_id=array();
                                        $app_start=array();
                                        $app_end=array();

                                        $daynum = date("w", strtotime($dayname));
                                        echo "<div class='calendar' style='margin-left: $margin_left% '>";
                                        echo "<center style='background: $day_color[$daynum]; margin-bottom: 5%;'> $dayname $x</center>";
                                        for($i=0; $i<24; $i++){
                                            if($i<10){
                                                echo '<div class="time">0'.$i.':00 <hr/></div>';
                                                echo '<div class="time">0'.$i.':30 <hr/></div>';
                                            }else{
                                                echo '<div class="time">'.$i.':00 <hr/></div>';
                                                echo '<div class="time">'.$i.':30 <hr/></div>';
                                            }
                    
                                        }
                                        
                                        if ($title!="") {
                                            while($row = mysqli_fetch_assoc($title)) {
                                                array_push($app_title,  $row["apm_text"]);
                                                array_push($app_id,  $row["apm_id"]);
                                                array_push($app_start,  $row["apm_start"]);
                                                array_push($app_end,  $row["apm_end"]);

                                                array_push($all_title,  $row["apm_text"]);
                                                array_push($all_id,  $row["apm_id"]);
                                                array_push($all_start,  $row["apm_start"]);
                                                array_push($all_end,  $row["apm_end"]);
                                                array_push($all_date,  $row["apm_date"]);
                                            }
                                        }

                                        for($i=0; $i<sizeof($app_title);$i++){

                                            $index_start = date('H', strtotime($app_start[$i]));
                                            if($index_start=="00"){
                                                $index_start = 0;
                                            }
                                            $min_start =  date('i', strtotime($app_start[$i]));
                                            $plus_start=16;
                                            if($min_start >=10 && $min_start <20){
                                                $plus_start=26;
                                            }else if($min_start >=20 && $min_start <30){
                                                $plus_start=44;
                                            }else if($min_start==30){
                                                $plus_start=53;
                                            }else if($min_start >30 && $min_start <40){
                                                $plus_start=58;
                                            }else if($min_start >=40 && $min_start <50){
                                                $plus_start=70;
                                            }else if($min_start >=50){
                                                $plus_start=82;
                                            }
                                    
                                            $index_end = date('H', strtotime($app_end[$i]));
                                            $min_end =  date('i', strtotime($app_end[$i]));
                                            $plus_end=16;
                                            if($min_end >=10 && $min_end <20){
                                                $plus_end=26;
                                            }else if($min_end >=20 && $min_end <30){
                                                $plus_end=44;
                                            }else if($min_end==30){
                                                $plus_end=53;
                                            }else if($min_end >30 && $min_end <40){
                                                $plus_end=60;
                                            }else if($min_end >=40 && $min_end <50){
                                                $plus_end=70;
                                            }else if($min_end >=50){
                                                $plus_end=82;
                                            }
                                    
                                            
                                            echo '<script>';
                                            echo 'var apm_left = document.getElementsByTagName("hr")[0].offsetLeft + 17;'; //get left position'
                                            echo 'var apm_item = document.createElement("DIV");';
                                            echo 'var setTop = document.getElementsByClassName("time")['.($index_start*2).'].offsetTop;';
                                            echo 'var setEnd = document.getElementsByClassName("time")['.($index_end*2).'].offsetTop;';
                                            echo 'apm_item.style.height = (setEnd+'.$plus_end.')-(setTop + '.$plus_start.')+"px";';
                                            echo 'apm_item.style.background = "red";';    
                                            echo 'apm_item.style.top = setTop +'.$plus_start.' +"px";';    
                                            echo 'apm_item.className = "appointment";';  
                                            echo 'apm_item.style.left = apm_left + "px";'; //set left position
                                            echo 'apm_item.style.color = "white";'; //set font color
                                            echo 'apm_item.style.padding = "1%";'; //set padding
                                            echo 'apm_item.style.position = "absolute";'; //set position
                                            echo 'apm_item.style.textOverflow = "ellipsis";';
                                            echo 'apm_item.innerHTML = "<b>"+"'.$app_title[$i].'"+"</b><br/>"+"'.$app_start[$i].'"+" - "+"'.$app_end[$i].'";';
                                            echo 'document.getElementsByClassName("calendar")['.($count-1).'].appendChild(apm_item)';    
                                            echo '</script>'; 

                                            echo '<script>';
                                            for($z=0;$z<sizeof($all_title);$z++){
                                               echo "document.getElementsByClassName('appointment')[$z].onclick = function(){
                                                    show_app('$all_date[$z]',' $all_id[$z]', '$all_title[$z]', '$all_start[$z]', '$all_end[$z]');
                                                };";
                                            }
                                            echo '</script>';
                                        }

                                        
                                    }
                                    echo '</div>';
                                }

                            ?>
        </div>
    </div>

    <div id="myModal" class="modal">

        <div class="modal-content">
            <span class="close">&times;</span>
            <div id="apm"></div>
            <div id="start"></div>
            <div id="end"></div>
            <form action="./Appointment/delete_apm_weekview.php" method="POST">
                <input type="text" name="id" id="id" hidden>
                <input type="date" name="apm_date" id="apm_date" hidden>
                <input type="submit" value="Delete" class="btn btn-outline-danger">
            </form>
        </div>

    </div>
</body>
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
function show_app(date, id, text, start, end) {
    modal.style.display = "block";
    document.getElementById("apm").innerHTML = text;
    document.getElementById("id").value = id;
    document.getElementById("apm_date").value = date;
    document.getElementById("start").innerHTML = start;
    document.getElementById("end").innerHTML = end;
}

//When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

</html>